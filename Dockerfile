FROM php:5.6-apache
# install some extensions
RUN export DEBIAN_FRONTEND=noninteractive && apt-get update && apt-get install -y \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libmcrypt-dev \
        libpng12-dev \
        libxml2 \
        libxml2-dev \
        libicu-dev \
        wget \
        mysql-client \
        unzip \
        git \
        cron \
        vim \
        pdftk \
        rubygems \
        build-essential \
        inetutils-syslogd \
        libxrender1 \
        libfontconfig1 \
        libapache2-mod-rpaf \
        logrotate \
    && docker-php-ext-install -j$(nproc) iconv intl mcrypt opcache pdo pdo_mysql mysqli mysql mbstring soap xml zip \
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install -j$(nproc) gd
# adding some configurations for apache, php
ADD wkhtmltox/bin/wkhtmltopdf /usr/local/bin/wkhtmltopdf
ADD wkhtmltox/bin/wkhtmltoimage /usr/local/bin/wkhtmltoimage
ADD wkhtmltox/lib/libwkhtmltox.so /usr/local/lib/libwkhtmltox.so
ADD wkhtmltox/lib/libwkhtmltox.so.0 /usr/local/lib/libwkhtmltox.so.0
ADD wkhtmltox/lib/libwkhtmltox.so.0.12 /usr/local/lib/libwkhtmltox.so.0.12
ADD wkhtmltox/lib/libwkhtmltox.so.0.12.3 /usr/local/lib/libwkhtmltox.so.0.12.3
ADD php.ini /usr/local/etc/php/php.ini
ADD apache2.conf /etc/apache2/apache2.conf
ADD 000-default.conf /etc/apache2/sites-available/000-default.conf
ADD startup.sh /usr/local/startup.sh
ADD fake_sendmail.sh /usr/local/bin/fake_sendmail.sh
RUN chmod +x /usr/local/bin/fake_sendmail.sh

# Enable rewrite and install composer for use in symfony
RUN a2enmod rewrite && a2enmod ssl && mkdir /composer-setup && wget https://getcomposer.org/installer -P /composer-setup && php /composer-setup/installer --install-dir=/usr/bin && rm -Rf /composer-setup

RUN mkdir /var/www/html/web

RUN mkdir /var/www/cache
RUN chown www-data -R /var/www/cache

RUN groupadd -r node && useradd -r -g node node

# gpg keys listed at https://github.com/nodejs/node

RUN set -ex \
  && for key in \
    9554F04D7259F04124DE6B476D5A82AC7E37093B \
    94AE36675C464D64BAFA68DD7434390BDBE9B9C5 \
    0034A06D9D9B0064CE8ADF6BF1747F4AD2306D93 \
    FD3A5288F042B6850C66B31F09FE44734EB7990E \
    71DCFD284A79C3B38668286BC97EC7A07EDE3FC1 \
    DD8F2338BAE7501E3DD5AC78C273792F7D83545D \
    B9AE9905FFD7803F25714661B63B535A4C206CA9 \
    C4F0DFFF4E8C1A8236409D08E73BC641CC11F4C8 \
  ; do \
    gpg --keyserver ha.pool.sks-keyservers.net --recv-keys "$key"; \
  done

ENV NPM_CONFIG_LOGLEVEL info
ENV NODE_VERSION 4.6.2

RUN curl -SLO "https://nodejs.org/dist/v$NODE_VERSION/node-v$NODE_VERSION-linux-x64.tar.xz" \
  && curl -SLO "https://nodejs.org/dist/v$NODE_VERSION/SHASUMS256.txt.asc" \
  && gpg --batch --decrypt --output SHASUMS256.txt SHASUMS256.txt.asc \
  && grep " node-v$NODE_VERSION-linux-x64.tar.xz\$" SHASUMS256.txt | sha256sum -c - \
  && tar -xJf "node-v$NODE_VERSION-linux-x64.tar.xz" -C /usr/local --strip-components=1 \
  && rm "node-v$NODE_VERSION-linux-x64.tar.xz" SHASUMS256.txt.asc SHASUMS256.txt \
  && ln -s /usr/local/bin/node /usr/local/bin/nodejs

RUN gem install sass
RUN npm install --loglevel warn -g less && \
        npm install --loglevel warn -g uglifyjs && \
        npm install --loglevel warn -g uglifycss
CMD "/usr/local/startup.sh"
